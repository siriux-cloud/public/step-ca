#!/bin/bash

set -eo pipefail

SCRIPT_DIR="$( cd "$( dirname "${0}" )" && pwd )"

export STEPPATH=$(step path)

function siriux_ca_provisioner_oidc() {
    DOCKER_STEPCA_INIT_PROVISIONER_NAME="${DOCKER_STEPCA_INIT_PROVISIONER_NAME:-admin}"
    DOCKER_STEPCA_INIT_ADMIN_SUBJECT="${DOCKER_STEPCA_INIT_ADMIN_SUBJECT:-step}"

    SIRIUX_STEPCA_PROVISIONER_OIDC_NAME="${SIRIUX_STEPCA_PROVISIONER_OIDC_NAME}"
    SIRIUX_STEPCA_PROVISIONER_OIDC_CLIENT_ID="${SIRIUX_STEPCA_PROVISIONER_OIDC_CLIENT_ID}"
    SIRIUX_STEPCA_PROVISIONER_OIDC_CLIENT_SECRET="${SIRIUX_STEPCA_PROVISIONER_OIDC_CLIENT_SECRET}"
    SIRIUX_STEPCA_PROVISIONER_OIDC_CONFIGURATION_ENDPOINT="${SIRIUX_STEPCA_PROVISIONER_OIDC_CONFIGURATION_ENDPOINT}"

    if [ -z "${SIRIUX_STEPCA_PROVISIONER_OIDC_NAME}" ]; then
        echo "No se ha especificado el nombre del proveedor OIDC"
        return 0
    fi

    if [ "$(step ca provisioner list | jq "[ .[] | select( .type == \"OIDC\" and .name == \"${SIRIUX_STEPCA_PROVISIONER_OIDC_NAME}\")] | any")" = "true" ]; then
        echo "El proveedor OIDC ${SIRIUX_STEPCA_PROVISIONER_OIDC_NAME} ya existe, se procede a actualizarlo."
        step ca provisioner update "${SIRIUX_STEPCA_PROVISIONER_OIDC_NAME}" \
            --client-id "${SIRIUX_STEPCA_PROVISIONER_OIDC_CLIENT_ID}" \
            --client-secret "${SIRIUX_STEPCA_PROVISIONER_OIDC_CLIENT_SECRET}" \
            --configuration-endpoint "${SIRIUX_STEPCA_PROVISIONER_OIDC_CONFIGURATION_ENDPOINT}" \
            --admin-subject "${DOCKER_STEPCA_INIT_ADMIN_SUBJECT}" \
            --admin-provisioner "${DOCKER_STEPCA_INIT_PROVISIONER_NAME}" \
            --admin-password-file "${PWDPATH}" \
            --x509-template "${SCRIPT_DIR}/templates/ca-provider-oidc.tpl"
    else
        echo "El proveedor OIDC ${SIRIUX_STEPCA_PROVISIONER_OIDC_NAME} no existe, se procede a crearlo."
        step ca provisioner add "${SIRIUX_STEPCA_PROVISIONER_OIDC_NAME}" \
            --type oidc \
            --client-id "${SIRIUX_STEPCA_PROVISIONER_OIDC_CLIENT_ID}" \
            --client-secret "${SIRIUX_STEPCA_PROVISIONER_OIDC_CLIENT_SECRET}" \
            --configuration-endpoint "${SIRIUX_STEPCA_PROVISIONER_OIDC_CONFIGURATION_ENDPOINT}" \
            --admin-subject "${DOCKER_STEPCA_INIT_ADMIN_SUBJECT}" \
            --admin-provisioner "${DOCKER_STEPCA_INIT_PROVISIONER_NAME}" \
            --admin-password-file "${PWDPATH}" \
            --x509-template "${SCRIPT_DIR}/templates/ca-provider-oidc.tpl"
    fi
}

siriux_ca_provisioner_oidc
