{
        "subject": {
                {{ $found := false }}
                {{ range .SANs }}
                        {{ if eq .Type "email" }}
                                "commonName": {{ toJson .Value }}
                                {{ $found = true }}
                        {{ end }}
                {{ end }}
                {{ if not $found }}
                        "commonName": {{ toJson .Subject.CommonName }}
                {{ end }}
         },
        "sans": {{ toJson .SANs }},
{{- if typeIs "*rsa.PublicKey" .Insecure.CR.PublicKey }}
        "keyUsage": ["keyEncipherment", "digitalSignature"],
{{- else }}
        "keyUsage": ["digitalSignature"],
{{- end }}
        "extKeyUsage": ["serverAuth", "clientAuth"]
}
