FROM smallstep/step-ca:latest

USER root

# Instalamos gomplate para manipular templates
ARG GOMPLATE_VERSION="3.11.5"
RUN wget https://github.com/hairyhenderson/gomplate/releases/download/v${GOMPLATE_VERSION}/gomplate_linux-amd64 -O /usr/bin/gomplate && \
    chmod +x /usr/bin/gomplate  

# Istalamos jq 
RUN apk update \
 && apk add jq \
 && rm -rf /var/cache/apk/*

# Copiar archivos necesarios desarrollados.
COPY root/ /

RUN chmod +x /siriux-ca-provider-oidc.sh

USER step

ENTRYPOINT ["/bin/bash", "/siriux-entrypoint.sh"]
CMD exec /usr/local/bin/step-ca --password-file $PWDPATH $CONFIGPATH
